//
//  AppDelegate.swift
//  RubiksCube
//
//  Created by dc on 2021/5/29.
//

import UIKit

// 一些预先的配置信息
let _textNum = 5        // 配置纹理个数 m11,m22....mii
var _dpi3: CGFloat = 2  // 设备dpi
var _bgR: UInt8 = 0     // 背景颜色
var _bgG: UInt8 = 0
var _bgB: UInt8 = 0
var _sn2 = 13           // 打乱程度
let _sn3 = 17
let _dis: Float = 12    // 视角距离
var mData2 = Mdata()    // 一些配置
var mData3 = Mdata()

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        _dpi3 = UIScreen.main.nativeScale
        let bgColor = UIColor.systemGreen.cgColor
        let colorComponents = bgColor.components!
        _bgR = UInt8(colorComponents[0]*255.0)
        _bgG = UInt8(colorComponents[1]*255.0)
        _bgB = UInt8(colorComponents[2]*255.0)
        
        mData3.dis = 0.0 - _dis
        mData2.dis = 3.0 - _dis
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

