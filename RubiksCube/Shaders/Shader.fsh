//
//  Shader.fsh
//  ES2
//
//  Created by mengyun on 16/2/2.
//  Copyright © 2016年 mengyun. All rights reserved.
//
varying lowp vec2 TexCoordOut;
uniform sampler2D Texture; // New

void main()
{
    highp vec4 myC = texture2D(Texture, TexCoordOut);
    // myC.r = 1.0;
    highp float x = TexCoordOut.x;
    highp float y = TexCoordOut.y;
    
    // 画中心圆
    highp float centerSize = 0.04;
    for (highp float i=0.125; i<1.0; i+=0.25) {
        for (highp float j=0.125; j<1.0; j+=0.25) {
            if ((x-i)*(x-i) + (y-j)*(y-j) < centerSize*centerSize/4.0) {
                myC.r = 0.0;
                myC.b = 0.0;
                myC.g = 0.0;
                gl_FragColor = myC;
                return;
            }
            if ((x-i)*(x-i) + (y-j)*(y-j) < centerSize*centerSize) {
                myC.r = 1.0;
                myC.b = 1.0;
                myC.g = 1.0;
                gl_FragColor = myC;
                return;
            }
        }
    }
    
    gl_FragColor = myC;
}
