//
//  Shader.vsh
//  ES2
//
//  Created by mengyun on 16/2/2.
//  Copyright © 2016年 mengyun. All rights reserved.
//

attribute vec4 positionShader;

attribute vec2 TexCoordIn; // New
varying vec2 TexCoordOut; // New

uniform mat4 modelViewProjectionMatrixShader;
//uniform mat3 normalMatrix;

void main()
{
    gl_Position = modelViewProjectionMatrixShader * positionShader;
    
    TexCoordOut = TexCoordIn;
}
