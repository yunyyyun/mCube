//
//  MData.swift
//  MagicCube
//
//  Created by mengyun on 16/3/27.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import Foundation
import UIKit

class Mdata: NSObject, NSCoding {
    var currentTextureID: Int = 0

    var type: Int = 0 //2,3
    var dis: Float = -11.0 //
    var numberOfRevert = 0 //还原次数
    var revertSteps: [Int] = [0]
    var revertTimes: [Int] = [0]
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.currentTextureID as NSNumber, forKey: "currentTextureID")
        aCoder.encode(self.type as NSNumber, forKey: "type")
        aCoder.encode(self.revertSteps as [Int], forKey: "revertSteps")
        aCoder.encode(self.revertTimes as [Int], forKey: "revertTimes")
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        guard let currentTextureID = aDecoder.decodeObject(forKey: "currentTextureID") as? NSNumber else { return nil }
        guard let type = aDecoder.decodeObject(forKey: "type") as? NSNumber else { return nil }
        guard let revertSteps = aDecoder.decodeObject(forKey: "revertSteps") as? [Int] else { return nil }
        guard let revertTimes = aDecoder.decodeObject(forKey: "revertTimes") as? [Int] else { return nil }
        //mData = Mdata(currentTextureID: currentTextureID, shortestTime: shortestTime, minStepsNum: minStepsNum, type: type)
        self.currentTextureID = currentTextureID.intValue
        self.type = type.intValue
        self.revertSteps = revertSteps
        self.revertTimes = revertTimes
    }
}
