//
//  DiyViewController.swift
//  MagicCube
//
//  Created by mengyun on 16/3/25.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import UIKit

class DiyViewController: UIViewController, CALayerDelegate {
    var xNum: Int=0
    var yMax: Int=0
    var yMin: Int=0
    
    @IBOutlet var setView: UIView!
    @IBOutlet var headLabel: UILabel!
    @IBOutlet var lineView: UIView!
    
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    
    @IBOutlet var miniSetBtn: UIButton!
    @IBOutlet var magicSetBtn: UIButton!
    @IBOutlet var nearLabel: UILabel!
    @IBOutlet var sliderView: UISlider!
    @IBOutlet var farLabel: UILabel!
    
    @IBOutlet var scoreView: UIImageView!
    @IBOutlet var scoreHead: UILabel!
    @IBOutlet var magicTime: UILabel!
    @IBOutlet var magicStep: UILabel!
    @IBOutlet var miniTime: UILabel!
    @IBOutlet var miniStep: UILabel!
    @IBOutlet var time3x3x3: UILabel!
    @IBOutlet var steps3x3x3: UILabel!
    @IBOutlet var time2x2x2: UILabel!
    @IBOutlet var steps2x2x2: UILabel!
    
    @IBAction func slider(_ sender: AnyObject) {
        print(distanceSlider.value)
        mData3.dis = 0.0-distanceSlider.value
        mData2.dis = 3.0-distanceSlider.value
    }
    @IBOutlet var distanceSlider: UISlider!
    @IBOutlet var clearCache: UIButton!
    @IBAction func clearCache(_ sender: AnyObject) {
        clearCacheClickedNum+=1
    }

    var clearCacheClickedNum=0 {
        didSet {
            if clearCacheClickedNum>=9 {
                mData2=Mdata()
                mData2.type=2
                MC.saveObj("mdata2", value: mData2)
                mData3=Mdata()
                mData3.type=3
                MC.saveObj("mdata2", value: mData2)
                clearCache.alpha=0.8
                miniTime.text = "NA"
                miniStep.text = "NA"
                magicTime.text = "NA"
                magicStep.text = "NA"
                selectBtnID = 101
            }
        }
    }
    
    var selectBtnID = 101 {
        didSet {
            for v in setView.subviews {
                if v.tag > 100 {
                    if selectBtnID == v.tag {
                        v.layer.cornerRadius = 2.0
                        v.layer.borderWidth = 4.0
                        v.layer.borderColor = UIColor(red: 1.0, green: 0.4, blue: 0.4, alpha: 1).cgColor
                    } else {
                        v.layer.cornerRadius = 0.0
                        v.layer.borderWidth = 0.0
                    }
                }
            }
        }
    }
    
    @IBAction func Click(_ sender: AnyObject) {
        selectBtnID = sender.tag
    }

    @IBAction func set3(_ sender: AnyObject) {
        mData3.currentTextureID = selectBtnID - 101
        let m3 =  Mdata()
        m3.type = 3
        m3.currentTextureID = selectBtnID - 101
        MC.saveObj("mdata3", value: mData3)
        // generateImage(selectBtnID-101)
    }
    
    @IBAction func set2(_ sender: AnyObject) {
        mData2.currentTextureID = selectBtnID - 101
        let m2 =  Mdata()
        m2.type = 2
        m2.currentTextureID = selectBtnID - 101
        MC.saveObj("mdata2", value: mData2)
        // saveTextureImage(selectBtnID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // reLayout()
        scoreView.image=UIImage(named: "background")
        
        distanceSlider.value = _dis
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if mData2.revertTimes[0] != 0 {
            miniTime.text = NSString(format: "%.2lf(秒)", Float(getMinNum(mData2.revertTimes))/100.0) as String
        }
        if mData2.revertSteps[0] != 0 {
            miniStep.text = NSString(format: "%d(步)", getMinNum(mData2.revertSteps)) as String
        }
        if mData3.revertTimes[0] != 0 {
            magicTime.text = NSString(format: "%.2lf(秒)", Float(getMinNum(mData3.revertTimes))/100.0) as String
        }
        if mData3.revertSteps[0] != 0 {
            magicStep.text = NSString(format: "%d(步)", getMinNum(mData3.revertSteps)) as String
        }
        
        self.title = "我的设置"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
