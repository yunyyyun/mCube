//
//  Cube.swift
//  ES2
//
//  Created by mengyun on 16/2/3.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import GLKit
import OpenGLES

class Cube: NSObject {
    
    var vertices = [GLfloat](repeating: 0.0, count: 72) //pointX, pointY, pointZ
    var textureCoords = [GLfloat](repeating: 0.0, count: 48) //textureX, textureY
    var colors = [GLubyte](repeating: 0, count: 96)
    var row: GLint = 0
    var col: GLint = 0
    var layer: GLint = 0
    
    var mmm = GLKMatrix4Identity  
    
    func initVecticesData(_ row: GLint, col: GLint, layer: GLint) {
        let moveX =  MOVE * GLfloat(col-1)
        let moveY = -MOVE * GLfloat(row-1)
        let moveZ = -MOVE * GLfloat(layer-1)
        
        var j=0
        //var k=0
        //var vertices = [GLfloat](count: 120, repeatedValue: 0.0)
        let index = Int(row*3+col+layer*9)
        initTextureCoords()
        for i in 0...71 {
            if i%3==0 {
                vertices[i] = cubeVerticesData[j] + moveX
                j=j+1
                vertices[i+1] = cubeVerticesData[j] + moveY
                j=j+1
                vertices[i+2] = cubeVerticesData[j] + moveZ
                j=j+1
            }
        }

        for i in 0...47 {
            //if i%2==0 {
            textureCoords[i] = allTextureCoords[index][i]
            //textureCoords[i+1] = allTextureCoords[index][k++]
            //}
        }
        //print(vertices)
        self.row = row
        self.col = col
        self.layer = layer
        
        for i in 0...95 {
            colors[i]=0
        }
        var color: GLubyte=0
        for i in 0...95 {
            if i%16 == 0 {
                color=GLubyte(colorFlag)
                self.colors[i]=color
                self.colors[i+4]=color
                self.colors[i+8]=color
                self.colors[i+12]=color
                colorFlag+=1
            }
        }
    }
}
