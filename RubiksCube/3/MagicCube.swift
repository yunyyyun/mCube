//
//  BlockData.swift
//  ES2
//
//  Created by mengyun on 16/2/4.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import GLKit
import OpenGLES

class MagicCube {

    //var hidden:[[[Bool]]]!          //方块是否隐藏
//    var hidden = [[[Bool]]](count: 6, repeatedValue:[[Bool]](count: 6, repeatedValue:
//        [Bool](count: 6, repeatedValue:false)))
    
    var cubes = [Cube](repeating: Cube(), count: 27)
    var textureArray = [GLuint](repeating: 1, count: _textNum)  //纹理数组
    
    func initMagicCube() {
//        for i in 0...5 {
//            for j in 0...5 {
//                for k in 0...5 {
//                    hidden[i][j][k]=(i==0||i==5||j==0||j==5||k==0||k==5)
//                }
//            }
//        }
        for i in 0...2 {
            for j in 0...2 {
                for k in 0...2 {
                    let m_cubes=Cube()
                    m_cubes.initVecticesData(GLint(i), col: GLint(j), layer: GLint(k))
                    cubes[i*3+j+k*9] = m_cubes
                }
            }
        }
        for i in 1...5 {
            textureArray[i-1] = loadTextureEXT(i)
        }
        
//        int flag=0
//        for(int i = 0 ;i <27;i++) {
//            if (cubes[i]._layer != 0) {
//                cubes[i]._colors[0] = cubes[i]._colors[4] = cubes[i]._colors[8] = cubes[i]._colors[12] = flag;
//            }
//            if (cubes[i]._layer != 2) {
//                cubes[i]._colors[32] = cubes[i]._colors[36] = cubes[i]._colors[40] = cubes[i]._colors[44] = flag;
//            }
//            if (cubes[i]._row != 0) {
//                cubes[i]._colors[80] = cubes[i]._colors[84] = cubes[i]._colors[88] = cubes[i]._colors[92] = flag;
//            }
//            if (cubes[i]._row != 2) {
//                cubes[i]._colors[64] = cubes[i]._colors[68] = cubes[i]._colors[72] = cubes[i]._colors[76] = flag;
//            }
//            if (cubes[i]._col != 0) {
//                cubes[i]._colors[48] = cubes[i]._colors[52] = cubes[i]._colors[56] = cubes[i]._colors[60] = flag;
//            }
//            if (cubes[i]._col != 2) {
//                cubes[i]._colors[16] = cubes[i]._colors[20] = cubes[i]._colors[24] = cubes[i]._colors[28] = flag;
//            }
//        }
        let flag: GLubyte=0
        for i in 0...26 {
            if (cubes[i].layer != 0) {
                cubes[i].colors[0]=flag
                cubes[i].colors[4]=flag
                cubes[i].colors[8]=flag
                cubes[i].colors[12]=flag
            }
            if (cubes[i].layer != 2) {
                cubes[i].colors[32]=flag
                cubes[i].colors[36]=flag
                cubes[i].colors[40]=flag
                cubes[i].colors[44]=flag
            }
            if (cubes[i].row != 0) {
                cubes[i].colors[80]=flag
                cubes[i].colors[84]=flag
                cubes[i].colors[88]=flag
                cubes[i].colors[92]=flag
            }
            if (cubes[i].row != 2) {
                cubes[i].colors[64]=flag
                cubes[i].colors[68]=flag
                cubes[i].colors[72]=flag
                cubes[i].colors[76]=flag
            }
            if (cubes[i].col != 0) {
                cubes[i].colors[48]=flag
                cubes[i].colors[52]=flag
                cubes[i].colors[56]=flag
                cubes[i].colors[60]=flag
            }
            if (cubes[i].col != 2) {
                cubes[i].colors[16]=flag
                cubes[i].colors[20]=flag
                cubes[i].colors[24]=flag
                cubes[i].colors[28]=flag
            }
        }

    }
}
