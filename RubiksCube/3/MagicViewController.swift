//
//  MagicViewController.swift
//  Magic
//
//  Created by mengyun on 16/1/30.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import GLKit
import OpenGLES
import UIKit

//func BUFFER_OFFSET(_ i: Int) -> UnsafeRawPointer {
//    let p: UnsafeRawPointer? = nil
//    return p.advancedBy(i)
//}

func BUFFER_OFFSET(_ i: Int) -> UnsafeRawPointer {
    return UnsafeRawPointer(bitPattern: i)!
}

let UNIFORM_MODELVIEWPROJECTION_MATRIX = 0
let UNIFORM_NORMAL_MATRIX = 1
let UNIFORM_TEXTURE=2
let UNIFORM_COLOR=3
var uniforms = [GLint](repeating: 0, count: 4)

class MagicViewController: GLKViewController {
    
    var program: GLuint = 0
    var _isPaused=0
    
    var modelViewProjectionMatrix: GLKMatrix4 = GLKMatrix4Identity
    var normalMatrix: GLKMatrix3 = GLKMatrix3Identity
    var rotation: Float = 0.0
    
//    var vertexArray: GLuint = 0
//    var vertexBuffer: GLuint = 0
    
    var context: EAGLContext?
    var effect: GLKBaseEffect?
    
    //var cube: cubes!
    var magicCube: MagicCube!
    
    var rotateType = 0 //旋转类型，0为整体转
    var move_flag: Bool=true
    var point1: CGPoint!//手指屏幕坐标
    var point2: CGPoint!
    
//    var startVec3: [Float] = [Float](count: 3, repeatedValue: 0.0)//轨迹球坐标
//    var endVec3: [Float] = [Float](count: 3, repeatedValue: 0.0)
 //   var rotQuaternion: [Float] = [Float](count: 4, repeatedValue: 0.0)//旋转四元数
    var rotMat: GLKMatrix4 = GLKMatrix4Identity // 整个魔方的旋转矩阵
    var tmpMat: GLKMatrix4 = GLKMatrix4Identity // 上次拖拽后保存旋转的矩阵
    //var pickMagicCube:[[Int]] = [[Int]](count: 2, repeatedValue: [Int](count: 3, repeatedValue: 0))
    
    var currentPickPixel: [GLubyte] = [GLubyte](repeating: 0, count: 3)   //当前pick的颜色数据
    var pickPixels: [[GLubyte]] = [[GLubyte]](repeating: [GLubyte](repeating: 0, count: 3), count: 2)  //pick方块的颜色数据
    var pickFlags=0  //0表示选中方块，10表示选中一个
    
    var rotationState=ROTATE_NONE
    var _RotateAngle: GLfloat=0
    var _isSelectMode: Bool=false
    
    var currentSlice = [GLint](repeating: -1, count: 3)
    
    //var mmm = [GLKMatrix4](count: 27, repeatedValue: GLKMatrix4Identity)
    var fff=0
    var ccc=0
//    GLubyte temp[16]
//    int tempqueue[56]GLint squence[56]
    
    var squence = [Int](repeating: 0, count: 56)
    
    //var testNum:GLuint=0
    
    //var timer=0   //帧计数
    var rotationAngle: GLfloat = -5.0 // 魔方旋转速度
    var round = _sn3 {
        didSet {
            //timeLabel.text=NSString(format: "time:%02d:%02d", totalMicrosecond/60, totalMicrosecond%60) as String
            promptLabel.isHidden = (round==0 )
        }
    }
    var timer: Timer!
    var totalMicrosecond = 0 {
        didSet {
            if totalMicrosecond<600000 {
                timeLabel.text=NSString(format: "time:%02d", totalMicrosecond/60) as String
            }
        }
    }
    var steps: Int = 0 {
        didSet {
            //steps=999
            stepLabel.text=NSString(format: "step:%02d", steps) as String
        }
    }
    
    var isReduction = 0 {
        didSet {
            if isReduction%10==1&&isReduction/10==0&&steps>0 {
//                promptLabel.text = "魔方已被你还原，恭喜你！"
//                promptLabel.hidden = false
                successLabel.isHidden=false
//                if steps<mData3.minStepsNum{
//                    mData3.minStepsNum=steps
//                }
//                if totalMicrosecond<mData3.shortestTime{
//                    mData3.shortestTime=totalMicrosecond
//                }
                
                if mData3.revertSteps[0] == 0 {
                    mData3.revertSteps[0]=steps
                    mData3.revertTimes[0]=totalMicrosecond
                } else {
                    //print("steps.........",steps)
                    mData3.revertSteps += [steps]
                    mData3.revertTimes += [totalMicrosecond]
                }
                //let data2 = getUserData(2, id: 2)
                //saveUserData(2, id: 2,cacheData: mData2)
                
                // Mdata.encode(mData3)
                MC.saveObj("mdata3", value: mData3)
                if steps != 0 {
                    isReduction=isReduction+10
                }
            } else {
                successLabel.isHidden=true
            }
        }
    }

    var timeLabel: UILabel!
    var stepLabel: UILabel!
    var promptLabel: UILabel!
    var successLabel: UILabel!
    
    deinit {
        self.tearDownGL()
        
        if EAGLContext.current() === self.context {
            EAGLContext.setCurrent(nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.context = EAGLContext(api: .openGLES2)
        if !(self.context != nil) {
            ////print("Failed to create ES context")
        }
        
        let view = self.view as! GLKView
        view.context = self.context!
        view.drawableDepthFormat = .format24
        
        rotMat = GLKMatrix4Multiply(GLKMatrix4MakeYRotation(-45), GLKMatrix4MakeXRotation(-45))
        tmpMat = rotMat
        
        self.setupGL()
        self.update()
        
        let reset = UIButton(type: .custom)
        reset.frame = CGRect(x: 5, y: 105, width: 50, height: 50)
        //reset.backgroundColor = UIColor.redColor()
        reset.setImage(UIImage(named: "Upset"), for: UIControl.State())
        reset.addTarget(self, action: #selector(MagicViewController.clickToSet(_:)), for: .touchUpInside)
        view.addSubview(reset)
        
        let width=view.frame.width
        let height=view.frame.height
        
        timeLabel = UILabel(frame: CGRect(x: width-80, y: 102, width: 80, height: 24))
        timeLabel.backgroundColor = UIColor.clear
        timeLabel.textColor = UIColor.gray
        timeLabel.text = "time:00"
        timeLabel.textAlignment = .left
        view.addSubview(timeLabel)
        
        stepLabel = UILabel(frame: CGRect(x: width-80, y: 126, width: 80, height: 24))
        stepLabel.backgroundColor = UIColor.clear
        stepLabel.textColor = UIColor.blue
        stepLabel.text = "step:0"
        stepLabel.textAlignment = .left
        view.addSubview(stepLabel)
        
        promptLabel = UILabel(frame: CGRect(x: 20, y: 166, width: width-40, height: 44))
        promptLabel.backgroundColor = UIColor.clear
        promptLabel.textColor = UIColor.red
        promptLabel.text = "请稍等,正在打乱魔方..."
        promptLabel.textAlignment = .center
        promptLabel.isHidden=true
        view.addSubview(promptLabel)
        
        successLabel = UILabel(frame: CGRect(x: 20, y: height-130, width: width-40, height: 44))
        successLabel.backgroundColor = UIColor.clear
        successLabel.textColor = UIColor.red
        successLabel.text = "魔方已被你还原，恭喜你！"
        successLabel.textAlignment = .center
        successLabel.isHidden=true
        view.addSubview(successLabel)
    }
    
    @objc func clickToSet(_ btn: UIButton) {
        //print("clicker")
        if round <= 0 {
            round=_sn3
            steps=0
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
            totalMicrosecond=0
        }
        isReduction=isReduction%10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.update()
        rotationState = ROTATE_NONE
        _isPaused=0
        let app = UIApplication.shared
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: app)
        
        self.title = "三阶魔方"
    }
    @objc func applicationWillEnterForeground(_ notification: Notification) {
        self.update()
        //rotationState = ROTATE_NONE
        _isPaused=0
        rotationState = ROTATE_NONE
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _isPaused=1
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        if self.isViewLoaded && (self.view.window != nil) {
            self.view = nil
            
            self.tearDownGL()
            
            if EAGLContext.current() === self.context {
                EAGLContext.setCurrent(nil)
            }
            self.context = nil
        }
    }
    
    func setupGL() {
        EAGLContext.setCurrent(self.context)
        _=self.loadShaders()
        glEnable(UInt32(GL_DEPTH_TEST))
        glEnable(UInt32(GL_SMOOTH))
        glActiveTexture(UInt32(GL_TEXTURE0))
        
        //cube=cubes()
        //cube.initVecticesData(1,col: 1, layer: 0)
        //cube.CubeSetupData()
        magicCube=MagicCube()
        magicCube.initMagicCube()
//        for i in 0...26 {
//            let cub=magicCube.cubes[i]
//            //print("setupGL......",cub.row,cub.col,cub.layer,"...|...",cub.colors[0],cub.colors[16],cub.colors[32],cub.colors[48],cub.colors[64],cub.colors[80])
//        }
        //magicCube.loadTexture()
    }
    
    func tearDownGL() {
        EAGLContext.setCurrent(self.context)
        
//        glDeleteBuffers(1, &vertexBuffer)
//        glDeleteVertexArraysOES(1, &vertexArray)
        
        self.effect = nil
        
        if program != 0 {
            glDeleteProgram(program)
            program = 0
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        point1=((touches as NSSet).anyObject()! as AnyObject).location(in: self.view)
        print("touchesBegan_:", point1 as Any)
        move_flag=false
        rotateType=rotateType(point1)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if rotateType == 0 {
            print("点击了背景，整体旋转", move_flag, rotationState)
        } else {
            print("点击了魔方", move_flag, rotationState)
        }
        
        point2=((touches as NSSet).anyObject()! as AnyObject).location(in: self.view)
        
        let a = (point2.x-point1.x)*(point2.x-point1.x)
        let b = (point2.y-point1.y)*(point2.y-point1.y)
        if (a + b > 0) {
            if move_flag && rotationState == ROTATE_NONE {
                if rotateType != 0 && round == 0 {
                    _isSelectMode=true
                    return
                }
                var startVec3: [Float] = [Float](repeating: 0.0, count: 3)//轨迹球坐标
                var endVec3: [Float] = [Float](repeating: 0.0, count: 3)
                startVec3=mapToSphere(point2)
                endVec3=mapToSphere(point1)
                var rotQuaternion: [Float] = [Float](repeating: 0.0, count: 4)
                rotQuaternion = getQuaternion(startVec3, endVec3: endVec3)
                rotMat = getRotationMatrix(rotQuaternion)
                rotMat = GLKMatrix4Multiply(rotMat, tmpMat)
            }
            move_flag=true
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        tmpMat=rotMat
//        if rotateType != 0 {
//            point2=(touches as NSSet).anyObject()!.locationInView(self.view)
//            _isSelectMode=true
//            return
//        }
//        if (move_flag == false) {
//        }
    }
    
    // 判断是否共rol,col或layer
    func checkIfCoplanar3(_ ii: [Int]) -> Bool {
        if ii.count != 9 {
            //print("error with checkIfCoplanar3")
            return false
        }
        let rRow = magicCube.cubes[ii[0]].row
        var checkIfEqualRow: Bool = true
        for i in 1...8 {
            if magicCube.cubes[ii[i]].row != rRow {
                checkIfEqualRow = false
                break
            }
        }
        if checkIfEqualRow {
            return true
        }
        
        let cCol = magicCube.cubes[ii[0]].col
        var checkIfEqualCol: Bool = true
        for i in 1...8 {
            if magicCube.cubes[ii[i]].col != cCol {
                checkIfEqualCol = false
                break
            }
        }
        if checkIfEqualCol {
            return true
        }
        
        let lLayer = magicCube.cubes[ii[0]].layer
        var checkIfEqualLayer: Bool = true
        for i in 1...8 {
            if magicCube.cubes[ii[i]].layer != lLayer {
                checkIfEqualLayer = false
                break
            }
        }
        if checkIfEqualLayer {
            return true
        }
        
        return false
    }
    
    //是否已经还原
    func checkStatus() -> Int {
        let l1: [Int] = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        let l2: [Int] = [9, 10, 11, 12, 13, 14, 15, 16, 17]
        let l3: [Int] = [18, 19, 20, 21, 22, 23, 24, 25, 26]
        
        let c1: [Int] = [0, 3, 6, 9, 12, 15, 18, 21, 24]
        let c2: [Int] = [1, 4, 7, 10, 13, 16, 19, 22, 25]
        let c3: [Int] = [2, 5, 8, 11, 14, 17, 20, 23, 26]
        
        let r1: [Int] = [0, 1, 2, 9, 10, 11, 18, 19, 20]
        let r2: [Int] = [3, 4, 5, 12, 13, 14, 21, 22, 23]
        let r3: [Int] = [6, 7, 8, 15, 16, 17, 24, 25, 26]
        
        if checkIfCoplanar3(l1)&&checkIfCoplanar3(l2)&&checkIfCoplanar3(l3)&&checkIfCoplanar3(c1)&&checkIfCoplanar3(c2)&&checkIfCoplanar3(c3)&&checkIfCoplanar3(r1)&&checkIfCoplanar3(r2)&&checkIfCoplanar3(r3)&&steps>0 {
            return (isReduction/10)*10+1
        }
        
        return (isReduction/10)*10
    }
    
    // 如果点击的是背景，则接下来旋转的是整个魔方
    func rotateType(_ point: CGPoint) -> Int {
        let viewport = UnsafeMutablePointer<GLint>.allocate(capacity: 4*MemoryLayout<GLint>.size)
        glGetIntegerv(UInt32(GL_VIEWPORT), viewport)
        let pixel = UnsafeMutablePointer<GLubyte>.allocate(capacity: 4*MemoryLayout<GLubyte>.size)
        let x: GLint = GLint(_dpi3 * point.x)
        let y: GLint = GLint(viewport[3]) - GLint(_dpi3 * point.y)
        glReadPixels(x, y, 1, 1, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), pixel)
        var rlt = 1
        if  pixel[2] == _bgB && pixel[1] == _bgG && pixel[0] == _bgR { //背景颜色
            rlt = 0
        }
        return rlt
    }
    
    // MARK: - GLKView and GLKViewController delegate methods
    
    @objc func update() {
        let aspect = fabsf(Float(self.view.bounds.size.width / self.view.bounds.size.height))
        let projectionMatrix=GLKMatrix4MakePerspective(GLKMathDegreesToRadians(45.0), aspect, 0.1, 100.0)
        var modelViewMatrix: GLKMatrix4=GLKMatrix4MakeTranslation(0.0, 0.0, mData3.dis)
        modelViewMatrix = GLKMatrix4Multiply(modelViewMatrix, rotMat)
        normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), nil)
        modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix)
    }
    
    // 每帧循环调用的方法
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        if _isPaused==1 {
            return
        }
        //let a=6
        // 打乱
        if round <= 0 {
            rotationAngle = -5.0
        } else if round>0 && rotationState == ROTATE_NONE {
            let i=Int(arc4random())%3
            let j=Int(arc4random())%3
            currentSlice[0] = -1
            currentSlice[1] = -1
            currentSlice[2] = -1
            currentSlice[i] = GLint(j)
            if (i==1) {
                rotationState = Int(arc4random())%2+1
            } else if (i==0) {
                rotationState = Int(arc4random())%2+1+2
            } else if (i==2) {
                rotationState = Int(arc4random())%2+1+4
            }
            rotationAngle = -10
        }
        glClearColor(GLclampf(_bgR)/255.0, GLclampf(_bgG)/255.0, GLclampf(_bgB)/255.0, 0.0)
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT) | GLbitfield(GL_DEPTH_BUFFER_BIT))
        // Render the object again with ES2
        //glUseProgram(program)
        
        if _isSelectMode && rotationState == ROTATE_NONE {
            //loadShaders()
            selectSlice()
            _isSelectMode=false
        }
        //_isSelectMode=true
        
        _=loadShaders()
        glUseProgram(program)
        glEnable(GLenum(GL_TEXTURE_2D))
        glEnable(GLenum(GL_BLEND))
        
        if (rotationState == ROTATE_X_CLOCKWISE || rotationState == ROTATE_Y_CLOCKWISE || rotationState == ROTATE_Z_CLOCKWISE) {
            _RotateAngle += rotationAngle
        } else if (rotationState == ROTATE_X_ANTICLOCKWISE || rotationState == ROTATE_Y_ANTICLOCKWISE || rotationState == ROTATE_Z_ANTICLOCKWISE) {
            _RotateAngle += -rotationAngle
        } else {
            _RotateAngle = 0
        }
        
        for i in 1...3 {
            for j in 1...3 {
                for k in 1...3 {
                    
                    let vertices=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].vertices
                    let textureCoords=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].textureCoords
                    //let colors=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].colors
                    ////print(colors)
                    glBindTexture(GLenum(GL_TEXTURE_2D), magicCube.textureArray[mData3.currentTextureID])
                    
                    glEnableVertexAttribArray(GLuint(GLKVertexAttrib.position.rawValue))
                    glVertexAttribPointer(GLuint(GLKVertexAttrib.position.rawValue), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, vertices)
                    
                    glEnableVertexAttribArray(GLuint(GLKVertexAttrib.normal.rawValue))
                    glVertexAttribPointer(GLuint(GLKVertexAttrib.normal.rawValue), 2, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, textureCoords)
                    
                    var rotMatrix = GLKMatrix4Identity
                    rotMatrix = magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].mmm
                    
//                    if _RotateAngle != 0 {
//                        print("now angle is: ", _RotateAngle)
//                    }
                    
                    if (currentSlice[2]>=0 && magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].layer == currentSlice[2] ) {
                        rotMatrix = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 0, 0, 1)
                        rotMatrix = GLKMatrix4Multiply(rotMatrix, magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].mmm)
                        if (_RotateAngle >= 90 || _RotateAngle <= -90) {
                            for m in 0 ..< 27 {
                                if (magicCube.cubes[m].layer == currentSlice[2]) {
                                    ////print("_RotateAngle;;;;;;;;",_RotateAngle,m)
                                    let ympMat = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 0, 0, 1)
                                    magicCube.cubes[m].mmm = GLKMatrix4Multiply(ympMat, magicCube.cubes[m].mmm)
                                }
                            }
                            //[GameLogic RotateWith:_currentSlice[2] rotationState:_rotationState cubes: Cubes]
                            Rotate(Int(currentSlice[2]), rotationState: rotationState)
                            _RotateAngle = 0
                            currentSlice[0] = -1
                            currentSlice[1] = -1
                            currentSlice[2] = -1
                            rotationState = ROTATE_NONE
                        }
                        //rotMat = GLKMatrix4MakeRotation(_RotateAngle*3.1416/180.0, 0, 0, 1)
                        isReduction = checkStatus()
                    }
                    if (magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].row == currentSlice[0] && currentSlice[0]>=0) {
                        rotMatrix = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 0, 1, 0)
                        rotMatrix = GLKMatrix4Multiply(rotMatrix, magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].mmm)
                        if (_RotateAngle >= 90 || _RotateAngle <= -90) {
                            for m in 0 ..< 27 {
                                if (magicCube.cubes[m].row == currentSlice[0]) {
                                    let ympMat = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 0, 1, 0)
                                    magicCube.cubes[m].mmm = GLKMatrix4Multiply(ympMat, magicCube.cubes[m].mmm)
                                }
                            }
                            Rotate(Int(currentSlice[0]), rotationState: rotationState)
                            _RotateAngle = 0
                            currentSlice[0] = -1
                            currentSlice[1] = -1
                            currentSlice[2] = -1
                            rotationState = ROTATE_NONE
                        }
                        //rotMat = GLKMatrix4MakeRotation(_RotateAngle*3.1416/2.0, 0, 1, 0)
                        isReduction = checkStatus()
                    }
                    if (magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].col == currentSlice[1] && currentSlice[1]>=0) {
                        rotMatrix = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 1, 0, 0)
                        rotMatrix = GLKMatrix4Multiply(rotMatrix, magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].mmm)
                        if (_RotateAngle >= 90 || _RotateAngle <= -90 ) {
                            for m in 0 ..< 27 {
                                if (magicCube.cubes[m].col == currentSlice[1]) {
                                    let ympMat = GLKMatrix4MakeRotation(_RotateAngle*GLfloat.pi/180.0, 1, 0, 0)
                                    magicCube.cubes[m].mmm = GLKMatrix4Multiply(ympMat, magicCube.cubes[m].mmm)
                                }
                            }
                            Rotate(Int(currentSlice[1]), rotationState: rotationState)
                            _RotateAngle = 0
                            currentSlice[0] = -1
                            currentSlice[1] = -1
                            currentSlice[2] = -1
                            rotationState = ROTATE_NONE
                        }
                        //rotMat = GLKMatrix4MakeRotation(_RotateAngle*3.1416/180.0, 1, 0, 0)
                        isReduction = checkStatus()
                    }
                   
                    let tempModeviewMatrix = modelViewProjectionMatrix
                    let temp = GLKMatrix4Multiply(tempModeviewMatrix, rotMatrix)
                    modelViewProjectionMatrix = temp
                    
                    withUnsafePointer(to: &modelViewProjectionMatrix, {
                        $0.withMemoryRebound(to: Float.self, capacity: 16, {
                            glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, $0)
                        })
                    })
                    glDrawArrays(GLenum(GL_TRIANGLE_STRIP), 0, GLsizei(24))
                    modelViewProjectionMatrix = tempModeviewMatrix
                }
            }
        }
    }
    
    func selectSlice() {
        rotationState=ROTATE_NONE
        var f1 = FACE_NONE
        var f2 = FACE_NONE
        _=loadShaders()
        glUseProgram(program)
        glDisable(GLenum(GL_TEXTURE_2D))
        glDisable(GLenum(GL_BLEND))
        glClearColor(GLclampf(_bgR)/255.0, GLclampf(_bgG)/255.0, GLclampf(_bgB)/255.0, 0.0)
        //glClear(GLbitfield(GL_COLOR_BUFFER_BIT) | GLbitfield(GL_DEPTH_BUFFER_BIT))
        let viewport = UnsafeMutablePointer<GLint>.allocate(capacity: 4*MemoryLayout<GLint>.size)
        glGetIntegerv(UInt32(GL_VIEWPORT), viewport)
//        for i in 0...8 {
//            ////print("after,,,,,,,",i, magicCube.cubes[i].row, magicCube.cubes[i].col, magicCube.cubes[i].layer)
//            //print("selectSlice,,,,,,,", magicCube.cubes[i].colors[0], magicCube.cubes[i].colors[16], magicCube.cubes[i].colors[32], magicCube.cubes[i].colors[48], magicCube.cubes[i].colors[64], magicCube.cubes[i].colors[80], magicCube.cubes[i].colors[88])
//        }
        
        for i in 1...3 {
            for j in 1...3 {
                for k in 1...3 {
                    
                    let vertices=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].vertices
                    //let textureCoords=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].textureCoords
                    let colors=magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].colors
                    ////print(colors)
                    glEnableVertexAttribArray(GLuint(GLKVertexAttrib.position.rawValue))
                    glVertexAttribPointer(GLuint(GLKVertexAttrib.position.rawValue), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, vertices)
                    
//                    glEnableVertexAttribArray(GLuint(GLKVertexAttrib.Normal.rawValue))
//                    glVertexAttribPointer(GLuint(GLKVertexAttrib.Normal.rawValue), 2, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, textureCoords)
                    
                    glEnableVertexAttribArray(GLuint(GLKVertexAttrib.color.rawValue))
                    glVertexAttribPointer(GLuint(GLKVertexAttrib.color.rawValue), 4, GLenum(GL_UNSIGNED_BYTE), 1, 0, colors)
                    
                    var rotMatrix = GLKMatrix4Identity
                    rotMatrix = magicCube.cubes[(i-1)*3+(j-1)+(k-1)*9].mmm
                    
                    let tempModeviewMatrix = modelViewProjectionMatrix
                    let temp = GLKMatrix4Multiply(tempModeviewMatrix, rotMatrix)
                    modelViewProjectionMatrix = temp
                    
                    withUnsafePointer(to: &modelViewProjectionMatrix, {
                        $0.withMemoryRebound(to: Float.self, capacity: 16, {
                            glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, $0)
                        })
                    })

                    glDrawArrays(GLenum(GL_TRIANGLE_STRIP), 0, GLsizei(24))
                    modelViewProjectionMatrix = tempModeviewMatrix
                }
            }
        }
        let pixel = UnsafeMutablePointer<GLubyte>.allocate(capacity: 4*MemoryLayout<GLubyte>.size)
        let tmpPixel = UnsafeMutablePointer<GLubyte>.allocate(capacity: 4*MemoryLayout<GLubyte>.size)
        let x: GLint=GLint(_dpi3*point1.x)
        let y: GLint=GLint(viewport[3])-GLint(_dpi3*point1.y)
        glReadPixels(x, y, 1, 1, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), pixel)
        //print("pixel", pixel[0], pixel[1], pixel[2])
        var cube1: Cube?
        var cube2: Cube?
        f1=FACE_NONE
        for i in 0...26 {
            for _j in 0..<6 {
                let j = _j*16
                ////print("FACE_NONE..................",i,j, magicCube.cubes[i].colors[j])
                if magicCube.cubes[i].colors[j] == pixel[0] {
                    f1 = (GLint(pixel[0]) - 1)%6
                    cube1 = magicCube.cubes[i]
                    break
                }
            }
            if cube1 != nil {
                //print("cube1",i,cube1!.row,cube1!.col,cube1!.layer)
                break
            }
        }
        
        if f1==FACE_NONE {
            ////print("..........face_none")
            rotationState = ROTATE_ALL
            currentSlice[0] = -1
            currentSlice[1] = -1
            currentSlice[2] = -1
        }
        var inc: GLint = 0
        var flag: GLint = 1
        var nextPoint: CGPoint
        repeat {
            nextPoint = getNextPoint(point1, point2: point2, inc: inc)
            ////print("points::::::::::::::::::::::::::::", inc, flag, point1, point2,nextPoint)
            //glReadPixels(nextPoint.x,viewport[3]-nextPoint.y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, tempPixelColor)
            //let pixel = UnsafeMutablePointer<GLubyte>.alloc(4*sizeof(GLubyte))
            let x: GLint=GLint(_dpi3*nextPoint.x)
            let y: GLint=GLint(viewport[3])-GLint(_dpi3*nextPoint.y)
            glReadPixels(x, y, 1, 1, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), tmpPixel)
            inc += flag
            if (pixel[0] == tmpPixel[0]) {
                continue
            }//pixel[2] == 0 && pixel[1] == 0 && pixel[0] == 25
            if tmpPixel[2] == _bgB && tmpPixel[1] == _bgG && tmpPixel[0] == _bgR {
                if (inc>0) {
                    inc = 0
                    flag = -1
                } else {
                    break
                }
            }
            f2 = FACE_NONE
            for i in 0...26 {
                for _j in 0..<6 {
                    let j = _j*16
                    if magicCube.cubes[i].colors[j] == tmpPixel[0] {
                        f2 = (GLint(tmpPixel[0]) - 1)%6
                        cube2 = magicCube.cubes[i]
                        break
                    }
                }
            }
            if cube2 != nil {
                ////print("cube2",f2,cube2!.row,cube2!.col,cube2!.layer)
            }
            if (f2 != FACE_NONE) {
                ////print(cube1!.row,cube1!.col,cube1!.layer,"__",cube2!.row,cube2!.col,cube2!.layer)
                checkRotationState(cube1!, face1: f1, _cube2: cube2!, face2: f2, flag: flag)
                ////print("checkRotationState.......................",f1,f2, rotationState,currentSlice)
                break
            }
        }while (rotationState == ROTATE_NONE)
        ////print("rotationState.......", rotationState,currentSlice)
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT) | GLbitfield(GL_DEPTH_BUFFER_BIT))
    }
    
    // MARK: - OpenGL ES 2 shader compilation
    //loadShaders()步骤：
    //1.创建程序。
    //2.创建并编译顶点着色器和片段着色器。
    //3.把 顶点着色器和片段着色器 与 程序连接起来。
    //4.设置 顶点着色器和片段着色器 的输入参数。
    //5.链接程序。
    //6.获取 uniform 指针。
    //注意：这步只能在5成功后才能调用，在linkProgrom前，uniform位置是不确定的。
    //7.断开 顶点着色器和片段着色器 ，并释放它们。
    //注意：程序并没释放。
    //
    //第4步是会变化的部分，第6步为可选。
    func loadShaders() -> Bool {
        var vertShader: GLuint = 0
        var fragShader: GLuint = 0
        var vertShaderPathname: String
        var fragShaderPathname: String
        
        //创建程序.
        program = glCreateProgram()
        
        //创建并编译顶点着色器.
        if _isSelectMode {
            vertShaderPathname = Bundle.main.path(forResource: "PickerShader", ofType: "vsh")!
            if self.compileShader(&vertShader, type: GLenum(GL_VERTEX_SHADER), file: vertShaderPathname) == false {
                print("Failed to compile vertex shader")
                return false
            }
            //创建并编译片段着色器.
            fragShaderPathname = Bundle.main.path(forResource: "PickerShader", ofType: "fsh")!
            if !self.compileShader(&fragShader, type: GLenum(GL_FRAGMENT_SHADER), file: fragShaderPathname) {
                print("Failed to compile fragment shader")
                return false
            }
        } else {
            vertShaderPathname = Bundle.main.path(forResource: "Shader", ofType: "vsh")!
            if self.compileShader(&vertShader, type: GLenum(GL_VERTEX_SHADER), file: vertShaderPathname) == false {
                print("Failed to compile vertex shader")
                return false
            }
            //创建并编译片段着色器.
            fragShaderPathname = Bundle.main.path(forResource: "Shader", ofType: "fsh")!
            if !self.compileShader(&fragShader, type: GLenum(GL_FRAGMENT_SHADER), file: fragShaderPathname) {
                print("Failed to compile fragment shader")
                return false
            }
        }
        
        // 把顶点着色器与程序连接起来.
        glAttachShader(program, vertShader)
        //把片段着色器与程序连接起来.
        glAttachShader(program, fragShader)
        
        //设置 顶点着色器和片段着色器 的输入参数。
        //"position"和"normal"与着色器代码Shader.vsh里面的2个attribute对应，
        //分别与setupGL加载的顶点数组里面的顶点和法线数据对应起来。
        glBindAttribLocation(program, GLuint(GLKVertexAttrib.position.rawValue), "positionShader")
        glBindAttribLocation(program, GLuint(GLKVertexAttrib.color.rawValue), "colorShader")
        
        //链接程序.
        if !self.linkProgram(program) {
            ////print("Failed to link program: \(program)")
            
            if vertShader != 0 {
                glDeleteShader(vertShader)
                vertShader = 0
            }
            if fragShader != 0 {
                glDeleteShader(fragShader)
                fragShader = 0
            }
            if program != 0 {
                glDeleteProgram(program)
                program = 0
            }
            
            return false
        }
        
        //获取 uniform 指针.
        uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(program, "modelViewProjectionMatrixShader")
        // uniforms[UNIFORM_COLOR] = glGetUniformLocation(program, "colorShader")
        
        //释放着色器
        if vertShader != 0 {
            glDetachShader(program, vertShader)
            glDeleteShader(vertShader)
        }
        if fragShader != 0 {
            glDetachShader(program, fragShader)
            glDeleteShader(fragShader)
        }
        
        return true
    }
    
    func compileShader(_ shader: inout GLuint, type: GLenum, file: String) -> Bool {
        var status: GLint = 0
        var source: UnsafePointer<Int8>
        do {
            source = try NSString(contentsOfFile: file, encoding: String.Encoding.utf8.rawValue).utf8String!
        } catch {
            ////print("Failed to load vertex shader")
            return false
        }
        //var castSource = UnsafePointer<GLchar>(source)
        var castSource: UnsafePointer<GLchar>? = UnsafePointer<GLchar>(source)
        shader = glCreateShader(type)
        glShaderSource(shader, 1, &castSource, nil)
        glCompileShader(shader)
        
        //#if defined(DEBUG)
        //        var logLength: GLint = 0
        //        glGetShaderiv(shader, GLenum(GL_INFO_LOG_LENGTH), &logLength)
        //        if logLength > 0 {
        //            var log = UnsafeMutablePointer<GLchar>(malloc(Int(logLength)))
        //            glGetShaderInfoLog(shader, logLength, &logLength, log)
        //            NSLog("Shader compile log: \n%s", log)
        //            free(log)
        //        }
        //#endif
        
        glGetShaderiv(shader, GLenum(GL_COMPILE_STATUS), &status)
        if status == 0 {
            glDeleteShader(shader)
            return false
        }
        return true
    }
    
    func linkProgram(_ prog: GLuint) -> Bool {
        var status: GLint = 0
        glLinkProgram(prog)
        
        //#if defined(DEBUG)
        //        var logLength: GLint = 0
        //        glGetShaderiv(shader, GLenum(GL_INFO_LOG_LENGTH), &logLength)
        //        if logLength > 0 {
        //            var log = UnsafeMutablePointer<GLchar>(malloc(Int(logLength)))
        //            glGetShaderInfoLog(shader, logLength, &logLength, log)
        //            NSLog("Shader compile log: \n%s", log)
        //            free(log)
        //        }
        //#endif
        
        glGetProgramiv(prog, GLenum(GL_LINK_STATUS), &status)
        if status == 0 {
            return false
        }
        
        return true
    }
    
    func validateProgram(_ prog: GLuint) -> Bool {
        var logLength: GLsizei = 0
        var status: GLint = 0
        
        glValidateProgram(prog)
        glGetProgramiv(prog, GLenum(GL_INFO_LOG_LENGTH), &logLength)
        if logLength > 0 {
            var log: [GLchar] = [GLchar](repeating: 0, count: Int(logLength))
            glGetProgramInfoLog(prog, logLength, &logLength, &log)
            ////print("Program validate log: \n\(log)")
        }
        
        glGetProgramiv(prog, GLenum(GL_VALIDATE_STATUS), &status)
        var returnVal = true
        if status == 0 {
            returnVal = false
        }
        return returnVal
    }

    func checkRotationState(_ _cube1: Cube, face1: GLint, _cube2: Cube, face2: GLint, flag: GLint) {
        var cube1=_cube1
        var cube2=_cube2
        if (flag == -1) {
            let temp = cube1
            cube1 = cube2
            cube2 = temp
        }
        let row1=cube1.row
        let col1=cube1.col
        let layer1=cube1.layer
        let row2=cube2.row
        let col2=cube2.col
        let layer2=cube2.layer
        if ((row1==row2 && col1 < col2 && layer1 == layer2 && layer1 == 0 && face1 == FACE_FRONT) ||
            (row1 == row2 && col1 == col2 && layer1 < layer2 && col1 == 2 && face1 == FACE_RIGHT) ||
            (row1 == row2 && col1 > col2 && layer1 == layer2 && layer1 == 2 && face1 == FACE_BACK) ||
            (row1 == row2 && col1 == col2 && layer1 > layer2 && col1 == 0 && face1 == FACE_LEFT) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_LEFT&&face2 == FACE_FRONT)||(face1 == FACE_FRONT&&face2 == FACE_RIGHT)||(face1 == FACE_RIGHT&&face2 == FACE_BACK)||(face1 == FACE_BACK&&face2 == FACE_LEFT))) ) {
                rotationState = ROTATE_Y_ANTICLOCKWISE
                currentSlice[0] = row1
                currentSlice[1] = -1
                currentSlice[2] = -1
        } else if ((row1 == row2 && col1 < col2 && layer1 == layer2 && layer1 == 2 && face1 == FACE_BACK) ||
            (row1 == row2 && col1 == col2 && layer1 < layer2 && col1 == 0 && face1 == FACE_LEFT) ||
            (row1 == row2 && col1 > col2 && layer1 == layer2 && layer1 == 0 && face1 == FACE_FRONT) ||
            (row1 == row2 && col1 == col2 && layer1 > layer2 && col1 == 2 && face1 == FACE_RIGHT) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_FRONT&&face2 == FACE_LEFT)||(face1 == FACE_RIGHT&&face2 == FACE_FRONT)||(face1 == FACE_BACK&&face2 == FACE_RIGHT)||(face1 == FACE_LEFT&&face2 == FACE_BACK)))) {
                rotationState = ROTATE_Y_CLOCKWISE
                currentSlice[0] = row1
                currentSlice[1] = -1
                currentSlice[2] = -1
        } else if ((row1 == row2 && col1 == col2 && layer1 < layer2 && row1 == 0  && face1 == FACE_TOP) ||
            (row1 > row2 && col1 == col2 && layer1 == layer2 && layer1 == 0  && face1 == FACE_FRONT) ||
            (row1 < row2 && col1 == col2 && layer1 == layer2 && layer1 == 2  && face1 == FACE_BACK) ||
            (row1 == row2 && col1 == col2 && layer1 > layer2 && row1 == 2  && face1 == FACE_BOTTOM) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_FRONT&&face2 == FACE_TOP)||(face1 == FACE_TOP&&face2 == FACE_BACK)||(face1 == FACE_BACK&&face2 == FACE_BOTTOM)||(face1 == FACE_BOTTOM&&face2 == FACE_FRONT)))) {
                rotationState = ROTATE_X_CLOCKWISE
                currentSlice[0] = -1
                currentSlice[1] = col1
                currentSlice[2] = -1
        } else if ((row1 == row2 && col1 == col2 && layer1 < layer2 && row1 == 2 && face1 == FACE_BOTTOM) ||
            (row1 > row2 && col1 == col2 && layer1 == layer2 && layer1 == 2 && face1 == FACE_BACK) ||
            (row1 < row2 && col1 == col2 && layer1 == layer2 && layer1 == 0 && face1 == FACE_FRONT) ||
            (row1 == row2 && col1 == col2 && layer1 > layer2 && row1 == 0 && face1 == FACE_TOP) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_TOP&&face2 == FACE_FRONT)||(face1 == FACE_BACK&&face2 == FACE_TOP)||(face1 == FACE_BOTTOM&&face2 == FACE_BACK)||(face1 == FACE_FRONT&&face2 == FACE_BOTTOM)))) {
                rotationState = ROTATE_X_ANTICLOCKWISE
                currentSlice[0] = -1
                currentSlice[1] = col1
                currentSlice[2] = -1
        } else if ((row1 == row2 && col1 < col2 && layer1 == layer2 && row1 == 0 && face1 == FACE_TOP) ||
            (row1 < row2 && col1 == col2 && layer1 == layer2 && col1 == 2 && face1 == FACE_RIGHT) ||
            (row1 == row2 && col1 > col2 && layer1 == layer2 && row1 == 2 && face1 == FACE_BOTTOM) ||
            (row1 > row2 && col1 == col2 && layer1 == layer2 && col1 == 0 && face1 == FACE_LEFT) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_TOP&&face2 == FACE_RIGHT)||(face1 == FACE_RIGHT&&face2 == FACE_BOTTOM)||(face1 == FACE_BOTTOM&&face2 == FACE_LEFT)||(face1 == FACE_LEFT&&face2 == FACE_TOP)))) {
                rotationState = ROTATE_Z_CLOCKWISE
                currentSlice[0] = -1
                currentSlice[1] = -1
                currentSlice[2] = layer1
        } else if ((row1 == row2 && col1 < col2 && layer1 == layer2 && row1 == 2 && face1 == FACE_BOTTOM) ||
            (row1 < row2 && col1 == col2 && layer1 == layer2 && col1 == 0 && face1 == FACE_LEFT) ||
            (row1 == row2 && col1 > col2 && layer1 == layer2 && row1 == 0 && face1 == FACE_TOP) ||
            (row1 > row2 && col1 == col2 && layer1 == layer2 && col1 == 2 && face1 == FACE_RIGHT) ||
            (row1 == row2 && col1 == col2 && layer1 == layer2 && ((face1 == FACE_RIGHT&&face2 == FACE_TOP)||(face1 == FACE_TOP&&face2 == FACE_LEFT)||(face1 == FACE_LEFT&&face2 == FACE_BOTTOM)||(face1 == FACE_BOTTOM&&face2 == FACE_RIGHT)))) {
                rotationState = ROTATE_Z_ANTICLOCKWISE
                currentSlice[0] = -1
                currentSlice[1] = -1
                currentSlice[2] = layer1
        }
    }
    
    //需要替换的方块和对应面
    func find(_ cubeIndex: Int, face: Int) {
        ////print("find...",cubeIndex, face)
        
        let _cubeIndex=GLint(cubeIndex)
        for i in 0...26 {
            if magicCube.cubes[i].row*3+magicCube.cubes[i].col == _cubeIndex-magicCube.cubes[i].layer*9 {
                ccc = i
                break
            }
        }
        
        for _i in 0..<6 {
            let i = _i*16
            let tmp = Int(magicCube.cubes[ccc].colors[i])-1
            if tmp%6 == face {
                fff = i/16
                return
            }
        }
        for _i in 0..<6 {
            let i = _i*16
            let tmp = Int(magicCube.cubes[ccc].colors[i])
            if tmp == 0 {
                fff = i/16
                return
            }
        }
    }
    
    //调整pickerColor和row,col, layer参数
    func changeColors(_ gap: Int) {
        var temp = [GLubyte](repeating: 0, count: 16)
        var tempqueue = [Int](repeating: 0, count: 56)
        
        //        var tempccc=0
        //        var tempfff=0
        for _i in 0..<7 {
            let i = _i*8
            find(squence[i]+gap, face: squence[i+1])
            ////print("yunyyyun1..",i,"...",ccc,fff)
            tempqueue[i]=ccc
            tempqueue[i+1]=fff*16
            
            find(squence[i+2]+gap, face: squence[i+3])
            ////print("yunyyyun2..",i,"...",ccc,fff)
            tempqueue[i+2]=ccc
            tempqueue[i+3]=fff*16
            
            find(squence[i+4]+gap, face: squence[i+5])
            ////print("yunyyyun3..",i,"...",ccc,fff)
            tempqueue[i+4]=ccc
            tempqueue[i+5]=fff*16
            
            find(squence[i+6]+gap, face: squence[i+7])
           // //print("yunyyyun4..",i,"...",ccc,fff)
            tempqueue[i+6]=ccc
            tempqueue[i+7]=fff*16
            
        }
        
        for _ii in 0..<7 {
            let ii = _ii*8
            for i in 0 ..< 16 {
                temp[i] = magicCube.cubes[tempqueue[ii]].colors[tempqueue[ii+1]+i]//color2[temp2+i]
            }
            for i in 0 ..< 16 {
                magicCube.cubes[tempqueue[ii]].colors[tempqueue[ii+1]+i] = magicCube.cubes[tempqueue[ii+2]].colors[tempqueue[ii+3]+i]//color2[temp2+i]
            }
            for i in 0 ..< 16 {
                magicCube.cubes[tempqueue[ii+2]].colors[tempqueue[ii+3]+i] = magicCube.cubes[tempqueue[ii+4]].colors[tempqueue[ii+5]+i]//color2[temp2+i]
            }
            for i in 0 ..< 16 {
                magicCube.cubes[tempqueue[ii+4]].colors[tempqueue[ii+5]+i] = magicCube.cubes[tempqueue[ii+6]].colors[tempqueue[ii+7]+i]//color2[temp2+i]
            }
            for i in 0 ..< 16 {
                magicCube.cubes[tempqueue[ii+6]].colors[tempqueue[ii+7]+i] = temp[i]//color2[temp2+i]
            }
        }

        var tr: GLint = 0
        var tc: GLint = 0
        var tl: GLint = 0
        var c1=0
        var c2=0
        var c3=0
        var c4=0
        for _i in 0..<2 {//替换row,col, layer参数,只需遍历前16个数据
            let i = _i*8
            find(squence[i]+gap, face: squence[i+1])
            ////print("yunyyyun111111(i).....",i,"(i)",ccc,fff)
            c1=ccc
            
            find(squence[i+2]+gap, face: squence[i+3])
           // //print("yunyyyun22222(i).....",i,"(i)",ccc,fff)
            c2=ccc
            
            find(squence[i+4]+gap, face: squence[i+5])
            ////print("yunyyyun333333(i).....",i,"(i)",ccc,fff)
            c3=ccc
            
            find(squence[i+6]+gap, face: squence[i+7])
            ////print("yunyyyun444444(i).....",i,"(i)",ccc,fff)
            c4=ccc
            
            tr = magicCube.cubes[c1].row
            tc = magicCube.cubes[c1].col
            tl = magicCube.cubes[c1].layer
            
            magicCube.cubes[c1].row = magicCube.cubes[c2].row
            magicCube.cubes[c1].col = magicCube.cubes[c2].col
            magicCube.cubes[c1].layer = magicCube.cubes[c2].layer
            
            magicCube.cubes[c2].row = magicCube.cubes[c3].row
            magicCube.cubes[c2].col = magicCube.cubes[c3].col
            magicCube.cubes[c2].layer = magicCube.cubes[c3].layer
            
            magicCube.cubes[c3].row = magicCube.cubes[c4].row
            magicCube.cubes[c3].col = magicCube.cubes[c4].col
            magicCube.cubes[c3].layer = magicCube.cubes[c4].layer
            
            magicCube.cubes[c4].row = tr
            magicCube.cubes[c4].col = tc
            magicCube.cubes[c4].layer = tl
            
        }
    }
    
    //层转替换数据
    func Rotate(_ rcl: Int, rotationState: Int) {
        //return
        if (rotationState ==  ROTATE_Z_ANTICLOCKWISE) {
            let _squence: [Int] = [1, 5, 3, 3, 7, 4, 5, 1,
                0, 3, 6, 4, 8, 1, 2, 5,
                0, 5, 6, 3, 8, 4, 2, 1,
                0, 0, 6, 0, 8, 0, 2, 0,
                1, 0, 3, 0, 7, 0, 5, 0,
                0, 2, 6, 2, 8, 2, 2, 2,
                1, 2, 3, 2, 7, 2, 5, 2]
            squence = _squence
            //print("ROTATE_Z_ANTICLOCKWISE",rcl)
            changeColors(rcl*9)
        } else if (rotationState == ROTATE_Z_CLOCKWISE ) {
            let _squence: [Int] = [1, 5, 5, 1, 7, 4, 3, 3,
                0, 3, 2, 5, 8, 1, 6, 4,
                0, 5, 2, 1, 8, 4, 6, 3,
                0, 0, 2, 0, 8, 0, 6, 0,
                1, 0, 5, 0, 7, 0, 3, 0,
                0, 2, 2, 2, 8, 2, 6, 2,
                1, 2, 5, 2, 7, 2, 3, 2]
            squence = _squence
            //print("ROTATE_Z_CLOCKWISE",rcl)
            changeColors(rcl*9)
        } else if (rotationState == ROTATE_Y_ANTICLOCKWISE ) {
            let _squence: [Int] = [1, 0, 11, 1, 19, 2, 9, 3,
                0, 3, 2, 0, 20, 1, 18, 2,
                0, 0, 2, 1, 20, 2, 18, 3,
                1, 5, 11, 5, 19, 5, 9, 5,
                0, 5, 2, 5, 20, 5, 18, 5,
                1, 4, 11, 4, 19, 4, 9, 4,
                0, 4, 2, 4, 20, 4, 18, 4]
            squence = _squence
            //print("ROTATE_Y_ANTICLOCKWISE",rcl)
          
            changeColors(rcl*3)
          
        } else if (rotationState == ROTATE_Y_CLOCKWISE ) {
            let _squence: [Int] = [1, 0, 9, 3, 19, 2, 11, 1,
                0, 3, 18, 2, 20, 1, 2, 0,
                0, 0, 18, 3, 20, 2, 2, 1,
                1, 5, 9, 5, 19, 5, 11, 5,
                0, 5, 18, 5, 20, 5, 2, 5,
                1, 4, 9, 4, 19, 4, 11, 4,
                0, 4, 18, 4, 20, 4, 2, 4]
            squence = _squence
            //print("ROTATE_Y_CLOCKWISE",rcl)
            changeColors(rcl*3)
        } else if (rotationState == ROTATE_X_ANTICLOCKWISE) {
            let _squence: [Int] = [9, 5, 3, 0, 15, 4, 21, 2,
                6, 4, 24, 2, 18, 5, 0, 0,
                6, 0, 24, 4, 18, 2, 0, 5,
                3, 3, 15, 3, 21, 3, 9, 3,
                0, 3, 6, 3, 24, 3, 18, 3,
                3, 1, 15, 1, 21, 1, 9, 1,
                0, 1, 6, 1, 24, 1, 18, 1]
            squence = _squence
            //print("ROTATE_X_ANTICLOCKWISE",rcl)
            changeColors(rcl)
        } else if (rotationState == ROTATE_X_CLOCKWISE ) {
            let _squence: [Int] = [9, 5, 21, 2, 15, 4, 3, 0,
                0, 0, 18, 5, 24, 2, 6, 4,
                0, 5, 18, 2, 24, 4, 6, 0,
                3, 3, 9, 3, 21, 3, 15, 3,
                0, 3, 18, 3, 24, 3, 6, 3,
                3, 1, 9, 1, 21, 1, 15, 1,
                0, 1, 18, 1, 24, 1, 6, 1]
            squence = _squence
            //print("ROTATE_X_CLOCKWISE",rcl)
            changeColors(rcl)
        }
        if round<=0 {
            steps+=1
            if timer==nil {
                totalMicrosecond=1
                timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(MagicViewController.tick(_:)), userInfo: nil, repeats: true)
            }
        } else {
            if round==1 {
                totalMicrosecond=0
                steps=0
            }
            round-=1
        }
    }
    @objc func tick(_ paramTimer: Timer) {
        if isReduction%10==0 {
            totalMicrosecond+=1
        }
    }
}
