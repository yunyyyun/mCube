//
//  TestVertexData.swift
//  ES2
//
//  Created by mengyun on 16/2/15.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import Foundation
import OpenGLES

let MOVE: GLfloat=1.0
var colorFlag=1
var allTextureCoords = [[GLfloat]](repeating: [GLfloat](repeating: 0.0, count: 48), count: 27)

let ROTATE_NONE = -1
let ROTATE_ALL = 0
let ROTATE_X_CLOCKWISE = 1      // 绕 x 轴， 顺时针
let ROTATE_X_ANTICLOCKWISE = 2  // 绕 x 轴， 逆时针
let ROTATE_Y_CLOCKWISE = 3
let ROTATE_Y_ANTICLOCKWISE = 4
let ROTATE_Z_CLOCKWISE = 5
let ROTATE_Z_ANTICLOCKWISE = 6

let FACE_NONE: GLint = -1
let FACE_FRONT: GLint = 0
let FACE_RIGHT: GLint = 1
let FACE_BACK: GLint   = 2
let FACE_LEFT: GLint   = 3
let FACE_BOTTOM: GLint = 4
let FACE_TOP: GLint   = 5

var cubeVerticesData: [GLfloat] = [
    -0.5, -0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5,     // Front face
    0.5, 0.5, 0.5, 0.5, -0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, -0.5,     // Right face
    0.5, -0.5, -0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, 0.5, -0.5,     // Back face
    -0.5, 0.5, -0.5, -0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, 0.5,     // Left face
    -0.5, -0.5, 0.5, -0.5, -0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, -0.5,     // Bottom face
    -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5      // Top Face
    ]

var FC: [GLfloat] = [1.00/4, 3/4.0, 1.00/4, 2.00/4, 2.00/4, 3/4.0, 2.00/4, 2.00/4]  //1 Front face
var RC: [GLfloat] = [2.00/4, 2.00/4, 1.00/4, 2.00/4, 2.00/4, 1.00/4, 1.00/4, 1.00/4]  //2 Right face
var BC: [GLfloat] = [1.00/4, 1.00/4, 1.00/4, 0.00, 2.00/4, 1.00/4, 2.00/4, 0.00]  //4 Back face
var LC: [GLfloat] = [1.00/4, 3/4.0, 0.00, 3/4.0, 1.00/4, 2.00/4, 0.00, 2.00/4]  //4 Left face
var DC: [GLfloat] = [1.00/4, 2.00/4, 0.00, 2.00/4, 1.00/4, 1.00/4, 0.00, 1.00/4]  //5  down face
var TC: [GLfloat] = [0.00, 1.00/4, 0.00, 0.00, 1.00/4, 1.00/4, 1.00/4, 0.00]  //6  Top face
var NC: [GLfloat] = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]  //no color

var textureCoordsOld: [[GLfloat]] = [
    FC,NC,NC,LC,NC,TC,   FC,NC,NC,NC,NC,TC,   FC,RC,NC,NC,NC,TC,   FC,NC,NC,LC,NC,NC,
    FC,NC,NC,NC,NC,NC,   FC,RC,NC,NC,NC,NC,   FC,NC,NC,LC,DC,NC,   FC,NC,NC,NC,DC,NC,
    FC,RC,NC,NC,DC,NC,   NC,NC,NC,LC,NC,TC,   NC,NC,NC,NC,NC,TC,   NC,RC,NC,NC,NC,TC,
    NC,NC,NC,LC,NC,NC,   NC,NC,NC,NC,NC,NC,   NC,RC,NC,NC,NC,NC,   NC,NC,NC,LC,DC,NC,
    NC,NC,NC,NC,DC,NC,   NC,RC,NC,NC,DC,NC,   NC,NC,BC,LC,NC,TC,   NC,NC,BC,NC,NC,TC,
    NC,RC,BC,NC,NC,TC,   NC,NC,BC,LC,NC,NC,   NC,NC,BC,NC,NC,NC,   NC,RC,BC,NC,NC,NC,
    NC,NC,BC,LC,DC,NC,   NC,NC,BC,NC,DC,NC,   NC,RC,BC,NC,DC,NC
]

//var Blue: [GLubyte] = [3, 3, 253]
//var Red: [GLubyte] = [253, 3, 3]
//var Green: [GLubyte] = [3, 253, 3]
//var Gray: [GLubyte] = [223, 223, 253]
//var Yello: [GLubyte] = [253, 243, 73]
//var Brown: [GLubyte] = [223, 123, 253]
//
//var faceColors: [[GLubyte]] = [Blue, Red, Green, Gray, Yello, Brown]

func initTextureCoords() {
    var ii=0
    for i in 0 ..< textureCoordsOld.count {
        for j in 0 ..< textureCoordsOld[i].count {
            allTextureCoords[ii/48][ii%48] = textureCoordsOld[i][j]
            ii += 1
        }
    }
}

var testCubeVerticesData: [GLfloat] = [
    -0.5, -0.5, 0.5, 1.00/4, 1.0, 0.5, -0.5, 0.5, 1.00/4, 2.00/4, -0.5, 0.5, 0.5, 2.00/4, 1.0, 0.5, 0.5, 0.5, 2.00/4, 2.00/4, 0.5, 0.5, 0.5, 1.0, 1.0, 0.5, -0.5, 0.5, 1.0, 1.0, 0.5, 0.5, -0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, -0.5, -0.5, -0.5, 1.0, 1.0, 0.5, 0.5, -0.5, 1.0, 1.0, -0.5, 0.5, -0.5, 1.0, 1.0, -0.5, 0.5, -0.5, 1.00/4, 1.0, -0.5, -0.5, -0.5, 0.0, 1.0, -0.5, 0.5, 0.5, 1.00/4, 2.00/4, -0.5, -0.5, 0.5, 0.0, 2.00/4, -0.5, -0.5, 0.5, 1.0, 1.0, -0.5, -0.5, -0.5, 1.0, 1.0, 0.5, -0.5, 0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, -0.5, 0.5, 0.5, 0.0, 1.00/4, 0.5, 0.5, 0.5, 0.0, 0.0, -0.5, 0.5, -0.5, 1.00/4, 1.00/4, 0.5, 0.5, -0.5, 1.00/4, 0.0]
