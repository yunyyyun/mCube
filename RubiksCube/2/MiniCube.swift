//
//  BlockData.swift
//  ES2
//
//  Created by mengyun on 16/2/4.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import GLKit
import OpenGLES

class MiniCube {
    
    var cubes = [CubeOfMini](repeating: CubeOfMini(), count: 8)
    var textureArray = [GLuint](repeating: 1, count: _textNum)  //纹理数组
    
    func initMagicCube() {
        for i in 0...1 {
            for j in 0...1 {
                for k in 0...1 {
                    let m_cubes=CubeOfMini()
                    m_cubes.initVecticesData(GLint(i), col: GLint(j), layer: GLint(k))
                    cubes[i*2+j+k*4] = m_cubes
                }
            }
        }
        
        for i in 1...5 {
            textureArray[i-1] = loadTextureEXT(i)
        }

        let flag: GLubyte=0
        for i in 0...7 {
            if (cubes[i].layer != 0) {
                cubes[i].colors[0]=flag
                cubes[i].colors[4]=flag
                cubes[i].colors[8]=flag
                cubes[i].colors[12]=flag
            }
            if (cubes[i].layer != 1) {
                cubes[i].colors[32]=flag
                cubes[i].colors[36]=flag
                cubes[i].colors[40]=flag
                cubes[i].colors[44]=flag
            }
            if (cubes[i].row != 0) {
                cubes[i].colors[80]=flag
                cubes[i].colors[84]=flag
                cubes[i].colors[88]=flag
                cubes[i].colors[92]=flag
            }
            if (cubes[i].row != 1) {
                cubes[i].colors[64]=flag
                cubes[i].colors[68]=flag
                cubes[i].colors[72]=flag
                cubes[i].colors[76]=flag
            }
            if (cubes[i].col != 0) {
                cubes[i].colors[48]=flag
                cubes[i].colors[52]=flag
                cubes[i].colors[56]=flag
                cubes[i].colors[60]=flag
            }
            if (cubes[i].col != 1) {
                cubes[i].colors[16]=flag
                cubes[i].colors[20]=flag
                cubes[i].colors[24]=flag
                cubes[i].colors[28]=flag
            }
        }

    }
    
}
