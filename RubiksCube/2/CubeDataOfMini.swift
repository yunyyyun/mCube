//
//  TestVertexData.swift
//  ES2
//
//  Created by mengyun on 16/2/15.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import Foundation
import OpenGLES

let MOVE2: GLfloat=1.0
var colorFlag2=1
var allTextureCoords2 = [[GLfloat]](repeating: [GLfloat](repeating: 0.0, count: 48), count: 8)

//
//let ROTATE_NONE2 = -1
//let ROTATE_ALL2 = 0
//let ROTATE_X_CLOCKWISE2 = 1
//let ROTATE_X_ANTICLOCKWISE2 = 2
//let ROTATE_Y_CLOCKWISE2 = 3
//let ROTATE_Y_ANTICLOCKWISE2 = 4
//let ROTATE_Z_CLOCKWISE2 = 5
//let ROTATE_Z_ANTICLOCKWISE2 = 6
//
//let FACE_NONE2: GLint = -1
//let FACE_FRONT2: GLint = 0
//let FACE_RIGHT2: GLint = 1
//let FACE_BACK2: GLint   = 2
//let FACE_LEFT2: GLint   = 3
//let FACE_BOTTOM2: GLint = 4
//let FACE_TOP2: GLint   = 5

var cubeVerticesData2: [GLfloat] = [
    -0.5, -0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5,     // Front face
    0.5, 0.5, 0.5, 0.5, -0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, -0.5,     // Right face
    0.5, -0.5, -0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, 0.5, -0.5,     // Back face
    -0.5, 0.5, -0.5, -0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, 0.5,     // Left face
    -0.5, -0.5, 0.5, -0.5, -0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, -0.5,     // Bottom face
    -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5      // Top Face
    ]

//var FC2: [GLfloat] = [0.33,1.00, 0.33,0.67, 0.67,1.00, 0.67,0.67]  //1 Front face
//var RC2: [GLfloat] = [0.67,0.67, 0.33,0.67, 0.67,0.33, 0.33,0.33]  //2 Right face
//var BC2: [GLfloat] = [0.33,0.33, 0.33,0.00, 0.67,0.33, 0.67,0.00]  //3 Back face
//var LC2: [GLfloat] = [0.33,1.00, 0.00,1.00, 0.33,0.67, 0.00,0.67]  //4 Left face
//var DC2: [GLfloat] = [0.33,0.67, 0.00,0.67, 0.33,0.33, 0.00,0.33]  //5  down face
//var TC2: [GLfloat] = [0.00,0.33, 0.00,0.00, 0.33,0.33, 0.33,0.00]  //6  Top face
//var NC2: [GLfloat] = [1.00,1.00, 1.00,1.00, 1.00,1.00, 1.00,1.00]  //no color

var FC2: [GLfloat] = [1.00/4, 3/4.0, 1.00/4, 2.00/4, 2.00/4, 3/4.0, 2.00/4, 2.00/4]  //1 Front face
var RC2: [GLfloat] = [2.00/4, 2.00/4, 1.00/4, 2.00/4, 2.00/4, 1.00/4, 1.00/4, 1.00/4]  //2 Right face
var BC2: [GLfloat] = [1.00/4, 1.00/4, 1.00/4, 0.00, 2.00/4, 1.00/4, 2.00/4, 0.00]  //4 Back face
var LC2: [GLfloat] = [1.00/4, 3/4.0, 0.00, 3/4.0, 1.00/4, 2.00/4, 0.00, 2.00/4]  //4 Left face
var DC2: [GLfloat] = [1.00/4, 2.00/4, 0.00, 2.00/4, 1.00/4, 1.00/4, 0.00, 1.00/4]  //5  down face
var TC2: [GLfloat] = [0.00, 1.00/4, 0.00, 0.00, 1.00/4, 1.00/4, 1.00/4, 0.00]  //6  Top face
var NC2: [GLfloat] = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]  //no color

var textureCoordsOld2: [[GLfloat]] = [
    FC2,NC2,NC2,LC2,NC2,TC2,  FC2,RC2,NC2,NC2,NC2,TC2,  FC2,NC2,NC2,LC2,DC2,NC2,   FC2,RC2,NC2,NC2,DC2,NC2,
    NC2,NC2,BC2,LC2,NC2,TC2,  NC2,RC2,BC2,NC2,NC2,TC2,  NC2,NC2,BC2,LC2,DC2,NC2,   NC2,RC2,BC2,NC2,DC2,NC2
]

//var Blue2: [GLubyte] = [3, 3, 253]
//var Red2: [GLubyte] = [253, 3, 3]
//var Green2: [GLubyte] = [3, 253, 3]
//var Gray2: [GLubyte] = [223, 223, 253]
//var Yello2: [GLubyte] = [253, 243, 73]
//var Brown2: [GLubyte] = [223, 123, 253]
//
//var faceColors2: [[GLubyte]] = [Blue2, Red2, Green2, Gray2, Yello2, Brown2]

func initTextureCoords2() {
    var ii=0
    for i in 0 ..< textureCoordsOld2.count {
        for j in 0 ..< textureCoordsOld2[i].count {
            allTextureCoords2[ii/48][ii%48] = textureCoordsOld2[i][j]
            ii += 1
        }
    }
}

var testCubeVerticesData2: [GLfloat] = [
    -0.5, -0.5, 0.5, 0.33, 1.0, 0.5, -0.5, 0.5, 0.33, 0.67, -0.5, 0.5, 0.5, 0.67, 1.0, 0.5, 0.5, 0.5, 0.67, 0.67, 0.5, 0.5, 0.5, 1.0, 1.0, 0.5, -0.5, 0.5, 1.0, 1.0, 0.5, 0.5, -0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, -0.5, -0.5, -0.5, 1.0, 1.0, 0.5, 0.5, -0.5, 1.0, 1.0, -0.5, 0.5, -0.5, 1.0, 1.0, -0.5, 0.5, -0.5, 0.33, 1.0, -0.5, -0.5, -0.5, 0.0, 1.0, -0.5, 0.5, 0.5, 0.33, 0.67, -0.5, -0.5, 0.5, 0.0, 0.67, -0.5, -0.5, 0.5, 1.0, 1.0, -0.5, -0.5, -0.5, 1.0, 1.0, 0.5, -0.5, 0.5, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 1.0, -0.5, 0.5, 0.5, 0.0, 0.33, 0.5, 0.5, 0.5, 0.0, 0.0, -0.5, 0.5, -0.5, 0.33, 0.33, 0.5, 0.5, -0.5, 0.33, 0.0]
