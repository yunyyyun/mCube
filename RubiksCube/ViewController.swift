//
//  ViewController.swift
//  RubiksCube
//
//  Created by dc on 2021/5/29.
//

import UIKit

class ViewController: UIViewController {

    let magicViewController = MagicViewController()
    let miniCubeViewController = MiniCubeViewController()
    let mainViewController = DiyViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "首页"
    }
    
    @IBAction func jump3(_ sender: Any) {
        self.navigationController?.pushViewController(magicViewController, animated: true)
    }
    
    @IBAction func jump2(_ sender: Any) {
        self.navigationController?.pushViewController(miniCubeViewController, animated: true)
    }
    
    @IBAction func jumpSet(_ sender: Any) {
        self.navigationController?.pushViewController(mainViewController, animated: true)
    }
}

