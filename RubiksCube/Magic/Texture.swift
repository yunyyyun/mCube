//
//  Texture.swift
//  MagicCube
//
//  Created by mengyun on 16/4/3.
//  Copyright © 2016年 mengyun. All rights reserved.
//

import Foundation
import GLKit
import OpenGLES

let normalIMGNum=8
let starNum=12
let flowerIMGNum=39

func loadTextureEXT(_ i: Int) -> GLuint {
    let textureImage=createCGImageEXT(i)
    let width=textureImage.width
    let height=textureImage.height
    //let textureData=calloc(width*height*4, sizeof(GLubyte))
    let textureData=UnsafeMutablePointer<GLubyte>.allocate(capacity: width*height*4*MemoryLayout<GLubyte>.size)
    //let bithiddenInfo = CGBithiddenInfo(rawValue: CGImageAlphaInfo.PremultipliedLast.rawValue)
    let textureContext=CGContext(data: textureData, width: width, height: height, bitsPerComponent: 8, bytesPerRow: width*4, space: textureImage.colorSpace!, bitmapInfo: UInt32(1))//todo
    
    textureContext?.draw(textureImage, in: CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height)))    
    var textureName: GLuint = 0
    glGenTextures(1, &textureName)
    glBindTexture(UInt32(GL_TEXTURE_2D), textureName)
    glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MIN_FILTER), GL_NEAREST)
    glTexImage2D(UInt32(GL_TEXTURE_2D), 0, GLint(GL_RGBA), GLsizei(width), GLsizei(height), 0, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), textureData)
    return textureName
}

func createCGImageEXT(_ index: Int) -> CGImage {
    let f = ["", "c", "d", "a", "b", "f"]
    let ff = f[index]+f[index]
    let f1 = f[index]+"1"
    let f2 = f[index]+"2"
    let f3 = f[index]+"3"
    let f4 = f[index]+"4"
    let f5 = f[index]+"5"
    let f6 = f[index]+"6"

    let img = UIImage(named: ff)
    let img1 = UIImage(named: f1)
    let img2 = UIImage(named: f2)
    let img3 = UIImage(named: f3)
    let img4 = UIImage(named: f4)
    let img5 = UIImage(named: f5)
    let img6 = UIImage(named: f6)
    return createTextureWithImageEXT(img1!, img2: img2!, img3: img3!, img4: img4!, img5: img5!, img6: img6!, targetIMG: img!).cgImage!
}

func createTextureWithImageEXT(_ img1: UIImage, img2: UIImage, img3: UIImage, img4: UIImage, img5: UIImage, img6: UIImage, targetIMG: UIImage) -> UIImage {
    let totalWidth = targetIMG.size.width
    let totalHeight = targetIMG.size.height
    let offScreeSize = CGSize(width: totalWidth, height: totalHeight)
    UIGraphicsBeginImageContext(offScreeSize)
    let width = totalWidth/4
    let height = totalWidth/4
    // print("createTextureWithImageEXT", totalWidth, totalHeight)
    
    let rect1 = CGRect(x: 0, y: 0, width: width, height: height)
    img1.draw(in: rect1)
    
    let rect2 = CGRect(x: 0, y: height, width: width, height: height)
    img2.draw(in: rect2)
    
    let rect3 = CGRect(x: 0, y: height*2, width: width, height: height)
    img3.draw(in: rect3)
    
    let rect4 = CGRect(x: width, y: 0, width: width, height: height)
    img4.draw(in: rect4)
    
    let rect5 = CGRect(x: width, y: height, width: width, height: height)
    img5.draw(in: rect5)
    
    let rect6 = CGRect(x: width, y: height*2, width: width, height: height)
    img6.draw(in: rect6)
    
    targetIMG.draw(in: CGRect(x: 0, y: 0, width: totalWidth, height: totalHeight))
    
    let textureImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return textureImage!
}
